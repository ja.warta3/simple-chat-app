package main

import (
    "fmt"
    "log"
    "net/http"
    "github.com/gorilla/websocket"
)

const port = ":10000"

//create a map to hold all the clients
var clients = make(map[string]*websocket.Conn)

// define an interface for receiving the JSON data from the client
var receivedJSONData map[string]interface{}

// Define the upgrader. This is the meat of the websockets - it upgrades the HTTP requests to websockets
var upgrader = websocket.Upgrader{
    ReadBufferSize:  1024,
    WriteBufferSize: 1024,
}

// this function will read incoming messages from the sockets
func socketReader(conn *websocket.Conn) {
    for {

        var message struct {
            Username string `json:"username"`
            Sendto string `json:"sendto"`
            Body string `json:"body"`
        }

        err := conn.ReadJSON(&message)
        if err != nil {
            fmt.Println(err)
            return
        }

        fmt.Println(message.Body)

        // in this block, I'm checking if a key exists in the map.
        // the _ means that if the key exists, it will be thrown away. If I put a var name here, then it would be stored in that variable
        // the ok variable will hold a true/false value if the key is found in the map or not
        if _, ok := clients[message.Username]; ok {
            //do something here
            fmt.Println("client exists")
        } else {
            fmt.Println("Client doesn't exist, saving now")
            clients[message.Username] = conn
        }

        if val, ok := clients[message.Sendto]; ok {
            //do something here
            fmt.Println("client exists, sending message")
            if err := val.WriteMessage(1, []byte(message.Body)); err != nil {
                fmt.Println(err)
                return
            }
        } else {
            fmt.Println("Sendto client doesn't exist")
        }
		
        // I've commented this out, but this reads in a basic "non-JSON" message.....
        /*
        messageType, p, err := conn.ReadMessage()
        if err != nil {
            log.Println(err)
            return
        }

        // print the message to the console
        fmt.Println(string(p))

        // build a response to the client and convert it to a byte array
        serverResponse := "Wow, I totally agree"
            serverResponseData := []byte(serverResponse)

        // send the message back to the client
        if err := conn.WriteMessage(messageType, serverResponseData); err != nil {
            log.Println(err)
            return
        }
        */
    }
}

func createWebsocket(w http.ResponseWriter, r *http.Request) {

    upgrader.CheckOrigin = func(r *http.Request) bool { return true }

    // upgrade the connection to a websocket
    ws, err := upgrader.Upgrade(w, r, nil)
    if err != nil {
        log.Println(err)
    }	
	
    log.Println("A client has connected")

    socketReader(ws)
}

func setupRoutes() {
    http.HandleFunc("/ws", createWebsocket)
}

func main() {
    fmt.Println("running the socket server")
    setupRoutes()
    log.Fatal(http.ListenAndServe(port, nil))
}
