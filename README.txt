## Simple Chat App
Running on EC2 instance
Domain name: rescuetheweather.com
Port: 10000

1. Port 10000 needs to be open (or change the port number in client.js and websockets.go)
2. Change the domain in client.js if using localhost instead of rescuetheweather.com
3. install golang packages on server:
    go get fmt
    go get github.com/gorilla/websocket
